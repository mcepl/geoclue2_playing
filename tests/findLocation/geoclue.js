#!/usr/bin/gjs
// Requires .desktop file installed in $XDG_DATA_DIRS/applications

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Geocode = imports.gi.GeocodeGlib;
const Lang = imports.lang;
const Mainloop = imports.mainloop;

const AccuracyLevel = {
    COUNTRY: 1,
    CITY: 4,
    NEIGHBORHOOD: 5,
    STREET: 6,
    EXACT: 8
};

const ManagerIface = '<node> \
  <interface name="org.freedesktop.GeoClue2.Manager"> \
    <method name="GetClient"> \
      <arg type="o" name="client" direction="out"/> \
    </method> \
  </interface> \
</node>';
const ManagerProxy = Gio.DBusProxy.makeProxyWrapper(ManagerIface);

const ClientInterface = '<node> \
<interface name="org.freedesktop.GeoClue2.Client"> \
    <property name="Location" type="o" access="read"/> \
    <property name="DesktopId" type="s" access="readwrite"/> \
    <property name="RequestedAccuracyLevel" type="u" access="readwrite"/> \
    <property name="DistanceThreshold" type="u" access="readwrite"/> \
    <property name="Active" type="b" access="read"/> \
    <method name="Start"/> \
    <method name="Stop"/> \
    <signal name="LocationUpdated"> \
        <arg name="old" type="o"/> \
        <arg name="new" type="o"/> \
    </signal> \
</interface> \
</node>';
const ClientProxy = Gio.DBusProxy.makeProxyWrapper(ClientInterface);

const LocationInterface = '<node> \
<interface name="org.freedesktop.GeoClue2.Location"> \
    <property name="Latitude" type="d" access="read"/> \
    <property name="Longitude" type="d" access="read"/> \
    <property name="Accuracy" type="d" access="read"/> \
    <property name="Description" type="s" access="read"/> \
</interface> \
</node>';
const LocationProxy = Gio.DBusProxy.makeProxyWrapper(LocationInterface);


function onLocationUpdated(proxy, sender, [oldPath, newPath]) {
  let geoclueLocation = new LocationProxy(Gio.DBus.system,
                                          "org.freedesktop.GeoClue2",
                                          newPath);
  let location = new Geocode.Location({ latitude: geoclueLocation.Latitude,
                                        longitude: geoclueLocation.Longitude,
                                        accuracy: geoclueLocation.Accuracy,
                                        description: geoclueLocation.Description });
  print('location:');
  print('\tlatitude = ' + geoclueLocation.Latitude);
  print('\tlongitude = ' + geoclueLocation.Longitude);
  print('\taccuracy = ' + geoclueLocation.Accuracy);
  print('\tdescription = ' + geoclueLocation.Description);
  //this._updateLocation(location);
  Mainloop.quit();
}

let _managerProxy = new ManagerProxy(Gio.DBus.system,
  'org.freedesktop.GeoClue2', '/org/freedesktop/GeoClue2/Manager');

let [clientAddr] = _managerProxy.GetClientSync();

let clientProxy = new ClientProxy(Gio.DBus.system,
    'org.freedesktop.GeoClue2', clientAddr);
clientProxy.DesktopId = 'geoclue-test-simple';
clientProxy.DistanceThreshold = 10000;
clientProxy.RequestedAccuracyLevel = AccuracyLevel.EXACT;
let updatedId = clientProxy.connectSignal('LocationUpdated',
    onLocationUpdated);
clientProxy.StartRemote();

Mainloop.run();
