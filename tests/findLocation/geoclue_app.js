#!/usr/bin/gjs

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Lang = imports.lang;

const ClientIface = '<node> \
  <interface name="org.freedesktop.GeoClue2.Manager"> \
    <method name="GetClient"> \
      <arg type="o" name="client" direction="out"/> \
    </method> \
  </interface> \
</node>';

const ClientProxy = Gio.DBusProxy.makeProxyWrapper(ClientIface);
var main_loop = new GLib.MainLoop(null, true);

let TestApp = new Lang.Class({
    Name: 'TestApp',
    Extends: Gio.Application,

    _init: function() {
        this.parent({//application_id: 'gnome-maps',
            flags: Gio.ApplicationFlags.NON_UNIQUE });

        this._clientProxy = new ClientProxy(Gio.DBus.system,
          'org.freedesktop.GeoClue2',
          '/org/freedesktop/GeoClue2/Manager');

    this._clientProxy.GetClientRemote(Lang.bind(this, this._getClient));
    },

    _getClient: function(result, error) {
      let client_str = result;
      
    },

    vfunc_activate: function() {
        this.hold();
    },

});

let main = function () {
    let app = new TestApp();
    app.run(ARGV);
    main_loop.run();
};

main();

